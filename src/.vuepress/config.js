module.exports = {
  title: 'La vie de Jérémie',
  themeConfig: {
    nav: [
      {
        text: 'Accueil',
        link: '/'
      },
      {
        text: 'Guide',
        link: '/guide/'
      },
      {
        text: 'Blue',
        link: '/blue.html'
      },
      {
        text: 'Un super site !',
        link: 'http://www.directwind.com/'
      }
    ],
  },
}
